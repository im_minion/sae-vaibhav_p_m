from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import nav_test
import pyrebase
import math
import json
import requests
import re

'''
e = 1
vg = 2
g = 3
o = 4
p = 5
vp = 6

Grammar:
y = 1
n = 0
'''


def givVal(model_answer, keywords, answer, out_of):
    # KEYWORDS =>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # TODO : Enhacnce this thing
    if (len(answer.split())) <= 5:
        return 0

    count = 0
    keywords_count = len(keywords)
    for i in range(keywords_count):
        if keywords[i] in answer:
            # print (keywords[i])
            count = count + 1
    k = 0
    if count == keywords_count:
        k = 1
    elif count == (keywords_count - 1):
        k = 2
    elif count == (keywords_count - 2):
        k = 3
    elif count == (keywords_count - 3):
        k = 4
    elif count == (keywords_count - 4):
        k = 5
    elif count == (keywords_count - 5):
        k = 6

    # GRAMMAR =>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    req = requests.get("https://api.textgears.com/check.php?text=" + answer + "&key=<write your own>")
    no_of_errors = len(req.json()['errors'])

    if no_of_errors > 5 or k == 6:
        g = 0
    else:
        g = 1

    # QST =>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # TODO- chk for shard shinde
    print("fuzz1 ratio: ", fuzz.ratio(model_answer, answer))
    q = math.ceil(fuzz.token_set_ratio(model_answer, answer) * 6 / 100)

    print("Keywords : ", k)
    print("Grammar : ", g)
    print("Qusestion Specific Things : ", q)

    predicted = nav_test.predict(k, g, q)
    result = predicted * out_of / 10
    return result[0]


config = {
    "apiKey": "<write your own>",
    "authDomain": "<write your own>",
    "databaseURL": "<write your own>",
    "projectId": "<write your own>",
    "storageBucket": "<write your own>",
    "messagingSenderId": "<write your own>"
}

firebsevar = pyrebase.initialize_app(config=config)
db = firebsevar.database()

model_answer1 = db.child("model_answers").get().val()[1]['answer']
out_of1 = db.child("model_answers").get().val()[1]['out_of']
keywords1 = db.child("model_answers").get().val()[1]['keywords']
keywords1 = re.findall(r"[a-zA-Z]+", keywords1)

model_answer2 = db.child("model_answers").get().val()[2]['answer']
out_of2 = db.child("model_answers").get().val()[2]['out_of']
keywords2 = db.child("model_answers").get().val()[2]['keywords']
keywords2 = re.findall(r"[a-zA-Z]+", keywords2)

model_answer3 = db.child("model_answers").get().val()[3]['answer']
out_of3 = db.child("model_answers").get().val()[3]['out_of']
keywords3 = db.child("model_answers").get().val()[3]['keywords']
keywords3 = re.findall(r"[a-zA-Z]+", keywords3)

all_answers = db.child("answers").get()

for each_users_answers in all_answers.each():
    # For the first answer ->
    print(each_users_answers.val()['email'])
    answer = each_users_answers.val()['a1']
    result = givVal(model_answer1, keywords1, answer, out_of1)
    db.child("answers").child(each_users_answers.key()).update({"result1": result})

    # For the Second answer ->
    answer = each_users_answers.val()['a2']
    result = givVal(model_answer2, keywords2, answer, out_of2)
    db.child("answers").child(each_users_answers.key()).update({"result2": result})

    # For the third answer ->
    answer = each_users_answers.val()['a3']
    result = givVal(model_answer3, keywords3, answer, out_of3)
    db.child("answers").child(each_users_answers.key()).update({"result3": result})
